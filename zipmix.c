#if 0 //To compile with VC, type "nmake zipmix.c" cool trick, eh? :)
zipmix.exe: zipmix.c; cl zipmix.c /O1 /G6 /MD /link /opt:nowin98
	del zipmix.obj
!if 0
#endif

#if defined(_WIN32)

#include <windows.h>
#include <conio.h>
#define USEFILECHAR 221

#else

#define _MAX_PATH 260
#define USEFILECHAR '*'
#define stricmp strcasecmp

#include <term.h>
int getch ()
{
	struct termios getchnsets, getchosets;
	char ch;

	tcgetattr(0,&getchosets);
	getchnsets = getchosets;
	getchnsets.c_lflag &= (~ICANON)&(~ECHO)&(~ISIG);
	getchnsets.c_cc[VMIN] = 1; getchnsets.c_cc[VTIME] = 0;
	tcsetattr(0,TCSANOW,&getchnsets);

	read(0,&ch,1);
	if (ch == 10) ch = 13; //Hack for portable CR

	tcsetattr(0,TCSANOW,&getchosets);

	return(ch);
}
#endif

#if defined(__APPLE__)
static unsigned long LSWAPIB (unsigned long a) { return(((a>>8)&0xff00)+((a&0xff00)<<8)+(a<<24)+(a>>24)); }
static unsigned short SSWAPIB (unsigned short a) { return((a>>8)+(a<<8)); }
#else
#define LSWAPIB(a) (a)
#define SSWAPIB(a) (a)
#endif

#include <stdio.h>
#include <string.h>

FILE *fil[3] = {0,0,0};
char filnam[3][_MAX_PATH];

#define TEMPBUFSIZ 65535
char tempbuf[TEMPBUFSIZ+1];

#define STARTMAXFILES 4096
long *filoffs[3] = {0,0,0}, *filhead[3] = {0,0,0}, fcnt[3] = {0,0,0}; //index order: read A, read B, write
#define STARTMAXHEADBUF 262144
char *filheadbuf = 0; long fheadcnt = 0, fheadmax = 0;

/*
	 [local file header] [file data]
				"               "
	 [central file header]
				"
	 [end of central directory record]

 Local file header:
	 local file header signature     4 bytes (0x04034b50)
 0  version needed to extract  ( 0) 2 bytes (20)
 1  general purpose bit flag   ( 2) 2 bytes (2)
 2  compression method         ( 4) 2 bytes (8)
 3  last mod file time         ( 6) 2 bytes (h<<11)+(m<<5)+(s>>1)
 4  last mod file date         ( 8) 2 bytes ((y-1980)<<9)+(m<<5)+(d)
 5  crc-32                     (10) 4 bytes (standard 0xdebb20e3, -1, ^-1)
 6  compressed size            (14) 4 bytes
 7  uncompressed size          (18) 4 bytes
 8  filename length            (22) 2 bytes (#)
 9  extra field length         (24) 2 bytes (0)
10  filename                   (26) # bytes (temp.txt)

Central file header:
	central file header signature    4 bytes (0x02014b50)
	version made by                  2 bytes (20)
 0 version needed to extract        2 bytes (20)
 1 general purpose bit flag         2 bytes (2)
 2 compression method               2 bytes (8)
 3 last mod file time               2 bytes (h<<11)+(m<<5)+(s>>1)
 4 last mod file date               2 bytes ((y-1980)<<9)+(m<<5)+(d)
 5 crc-32                           4 bytes (standard 0xdebb20e3, -1, ^-1)
 6 compressed size                  4 bytes
 7 uncompressed size                4 bytes
 8 filename length                  2 bytes (#)
 9 extra field length               2 bytes (0)
	file comment length              2 bytes (0)
	disk number start                2 bytes (0)
	internal file attributes         2 bytes (0:binary,1:text)
 ! external file attributes         4 bytes (0x20 or LSB=Dir Attrib Byte)
	relative offset of local header  4 bytes (points to: 0x04034b50)
10 filename                         # bytes (temp.txt)

End of central dir record:
	end of cent.dir. signature                  4 bytes (0x06054b50)
	# of this disk                              2 bytes (0)
	# of disk w/ the start of cent.dir.         2 bytes (0)
	total # entries in cent.dir. on this disk   2 bytes (# files)
	total # entries in cent.dir.                2 bytes (# files)
	size of cent.dir.                           4 bytes (0x06054b50-0x02014b50)
	offset: start cent.dir.rel. to start disk # 4 bytes (points to: 0x02014b50)
	zipfile comment length                      2 bytes (0)
*/

void quitout (long removefil)
{
	long i;

	if (filheadbuf) free(filheadbuf);
	for(i=2;i>=0;i--) if (filhead[i]) free(filhead[i]);
	for(i=2;i>=0;i--) if (filoffs[i]) free(filoffs[i]);
	if (fil[2]) fclose(fil[2]);
	if (fil[1]) fclose(fil[1]);
	if (fil[0]) fclose(fil[0]);
	if (removefil) remove(filnam[2]);
	exit(0);
}

long kstrnicmp (char *st0, char *st1, long i)
{
	char ch0, ch1;

	for(i--;i>=0;i--)
	{
		ch0 = st0[i]; if (((unsigned char)(ch0-97)) < 26) ch0 -= 32;
		ch1 = st1[i]; if (((unsigned char)(ch1-97)) < 26) ch1 -= 32;
		if (ch0 != ch1) return(-1);
	}
	return(0);
}

void appendfil (FILE *wfil, FILE *rfil, long leng)
{
	long i, j;

	for(i=0;i<leng;i+=TEMPBUFSIZ)
	{
		j = leng-i; if (j > TEMPBUFSIZ) j = TEMPBUFSIZ;
		fread(tempbuf,j,1,rfil);
		fwrite(tempbuf,j,1,wfil);
	}
}

void showhelp ()
{
	puts("ZIPMIX: maximize ZIP compression on file-by-file basis         by Ken Silverman");
	printf("Ken's official web site: http://advsys.net/ken            Compiled: %s\n\n",__DATE__);
	puts("ZIPMIX compares the files inside two ZIP files and copies any better compressed");
	puts("files that match in filename, CRC32, time, and uncompressed size. File version");
	puts("and order are taken from the first input file.\n");
	puts("Syntax:");
	puts("   zipmix [in(A)&out .ZIP] [in(B).ZIP]             (options)");
	puts("   zipmix [in(A)     .ZIP] [in(B).ZIP] [out .ZIP]  (options)\n");
	puts("Options:               | (none)  out=A     copy file from B only if smaller");
	puts("   /q: quiet operation | /or     out=A|B   combine 2 ZIPs, taking smallest");
	puts("   /y: ok to overwrite | /and    out=A&B   keep only files present in both ZIPs");
	puts("                       | /sub    out=A&~B  remove files with matching names");
}

enum { BOOL_NONE=0,BOOL_OR,BOOL_AND,BOOL_SUB };
int main (long argc, char **argv)
{
	unsigned long leng[2];
	long i, j, k, l, fileng[3], score[4], fmax, ftell43;
	long filespecified = 0, boolop = BOOL_NONE, quiet = 0, overwriteyesmode = 0;
	short s;
	char ch, *cptr, *cptr2;

	for(i=1;i<argc;i++)
	{
		if ((argv[i][0] == '/') || (argv[i][0] == '-'))
		{
			if (!stricmp(&argv[i][1],"q")) { quiet = 1; continue; }
			if (!stricmp(&argv[i][1],"y")) { overwriteyesmode = 1; continue; }
				  if (!stricmp(&argv[i][1],"or" )) { boolop = BOOL_OR;  continue; }
			else if (!stricmp(&argv[i][1],"and")) { boolop = BOOL_AND; continue; }
			else if (!stricmp(&argv[i][1],"sub")) { boolop = BOOL_SUB; continue; }
			if (!quiet) printf("Warning: ignoring unrecognized option: %s\n",&argv[i][0]);
			continue;
		}

		if (filespecified >= 3) { printf("Error: Too many ZIP files specified\n"); return(1); }
		strcpy(filnam[filespecified],argv[i]);
		for(j=0;filnam[filespecified][j];j++);
		while ((j) && (filnam[filespecified][j-1] != '\\')) j--;
		if (!strchr(&filnam[filespecified][j],'.')) strcat(filnam[filespecified],".ZIP");
		filespecified++;
	}
	if (filespecified < 2)
	{
		 if (filespecified == 1) printf("Error: must specify at least 2 ZIP files\n"); else showhelp();
		 return(1);
	}
	if (filespecified == 2)
	{
#if defined(_WIN32)
		cptr = _tempnam((char *)getenv("TMP"),"zipmix");
#else
		cptr = tmpnam(0);
#endif
		if (!cptr) { puts("Error: couldn't make tmp file"); quitout(0); }
		strcpy(filnam[2],cptr);
	}


	if ((!stricmp(filnam[0],filnam[2])) || (!stricmp(filnam[1],filnam[2])))
		{ puts("Error: input & output files must be different"); quitout(0); }
	if (!overwriteyesmode)
	{
		if (fil[2] = fopen(filnam[2],"r"))
		{
			fclose(fil[2]);
			printf("Overwrite %s? (Y/N)\n",filnam[2]);
			do
			{
				ch = getch();
			} while ((ch != 'Y') && (ch != 'y') && (ch != 'N') && (ch != 'n') && (ch != 27));
			if ((ch != 'Y') && (ch != 'y')) { puts("Error: aborted"); quitout(0); }
		}
	}
	if (!(fil[0] = fopen(filnam[0],"rb"))) { printf(tempbuf,"Error: %s not found\n",filnam[0]); quitout(0); }
	if (!(fil[1] = fopen(filnam[1],"rb"))) { printf(tempbuf,"Error: %s not found\n",filnam[1]); quitout(0); }
	if (!(fil[2] = fopen(filnam[2],"wb"))) { printf(tempbuf,"Error: can't write %s\n",filnam[2]); quitout(0); }

	score[0] = score[1] = score[2] = score[3] = 0;
	for(i=0;i<2;i++)
	{
		fseek(fil[i],0,SEEK_END); fileng[i] = ftell(fil[i]);
		fseek(fil[i],0,SEEK_SET);

		fmax = 0;

		while (1)
		{
			fread(&j,4,1,fil[i]); j = LSWAPIB(j);
			if (j == 0x02014b50) break;
			if ((j == 0x06054b50) && (!fcnt[i])) break; //Support ZIP files with 0 files inside :P
			if (j != 0x04034b50) { puts("Error: unexpected signature found"); quitout(1); }

			while (fcnt[i]+1 >= fmax)
			{
				fmax = max(fmax<<1,STARTMAXFILES);
				filoffs[i] = (long *)realloc(filoffs[i],fmax*sizeof(long)); if (!filoffs[i]) { puts("Error: too many files"); quitout(1); }
				filhead[i] = (long *)realloc(filhead[i],fmax*sizeof(long)); if (!filhead[i]) { puts("Error: too many files"); quitout(1); }
			}

			while (fheadmax-fheadcnt < 30+_MAX_PATH)
			{
				fheadmax = max(fheadmax<<1,STARTMAXHEADBUF);
				filheadbuf = (char *)realloc(filheadbuf,fheadmax*sizeof(char));
				if (!filheadbuf) { puts("Error: too many files"); quitout(1); }
			}

			cptr = &filheadbuf[fheadcnt];
			fread(cptr,26,1,fil[i]);
			k = SSWAPIB(*(unsigned short *)&cptr[22]);
			fread(&cptr[26],k,1,fil[i]); cptr[k+26] = 0;
			filhead[i][fcnt[i]] = fheadcnt; fheadcnt += k+26+1;

			fseek(fil[i],SSWAPIB(*(unsigned short *)&cptr[24]),SEEK_CUR);
			filoffs[i][fcnt[i]] = ftell(fil[i]);
			fseek(fil[i],LSWAPIB(*(unsigned long *)&cptr[14]),SEEK_CUR);

			fcnt[i]++;
		}
	}

	switch(boolop)
	{
		case BOOL_NONE: i = fcnt[0];              break; //C=A
		case BOOL_OR:   i = fcnt[0]+fcnt[1];      break; //C=A|B
		case BOOL_AND:  i = min(fcnt[0],fcnt[1]); break; //C=A&B
		case BOOL_SUB:  i = fcnt[0];              break; //C=A&~B
	}
	filoffs[2] = (long *)malloc(i*sizeof(long));
	filhead[2] = (long *)malloc(i*sizeof(long));

	if (!quiet) printf("%39s%20s\n",filnam[0],filnam[1]);

	fcnt[2] = 0;
	for(i=0;i<fcnt[0];i++)
	{
		cptr = &filheadbuf[filhead[0][i]];
		k = SSWAPIB(*(unsigned short *)&cptr[22]);
		if (!quiet) printf("%-20s",&cptr[26]);
		leng[0] = LSWAPIB(*(unsigned long *)&cptr[14]);

		ftell43 = ftell(fil[2]);
		for(j=0;j<fcnt[1];j++)
		{
			cptr2 = &filheadbuf[filhead[1][j]];

				//compare match
			if (SSWAPIB(*(unsigned short *)&cptr2[22]) != k) continue; //filename length
			if (kstrnicmp(&cptr[26],&cptr2[26],k))           continue; //filename
			if (boolop == BOOL_SUB) { if (!quiet) printf("\t\t\teliminated by /sub\n"); break; }
			if (*(long *)&cptr[18] != *(long *)&cptr2[18])   continue; //uncompressed size
			if (*(long *)&cptr[ 6] != *(long *)&cptr2[ 6])   continue; //time/date
			if (*(long *)&cptr[10] != *(long *)&cptr2[10])   continue; //crc32

			memcpy(&cptr2[26],&cptr[26],k); //Hack: use case of 1st filename even if 2nd smaller (added 12/28/2006)

			leng[1] = LSWAPIB(*(unsigned long *)&cptr2[14]);
			if (leng[0] <= leng[1]) //1st file <= 2nd file
			{
				if (leng[0] < leng[1]) { score[0]++; if (!quiet) printf("%19d%c%19d%20d\n",leng[0],USEFILECHAR,leng[1],leng[1]-leng[0]); } //2nd file is bigger
										else { score[2]++; if (!quiet) printf("%19d%c%19d\n"    ,leng[0],USEFILECHAR,leng[1]); } //file size tied
				l = LSWAPIB(0x04034b50); fwrite(&l,4,1,fil[2]);
				*(short *)&cptr[24] = 0; //set extra length to 0
				fwrite(&cptr[0],k+26,1,fil[2]);
				fseek(fil[0],filoffs[0][i],SEEK_SET);
				appendfil(fil[2],fil[0],leng[0]);
				filoffs[2][fcnt[2]] = ftell43;
				filhead[2][fcnt[2]] = filhead[0][i]; fcnt[2]++;
			}
			else //1st file > 2nd file
			{
				score[1]++; if (!quiet) printf("%19d%20d%c%19d\n",leng[0],leng[1],USEFILECHAR,leng[1]-leng[0]);
				l = LSWAPIB(0x04034b50); fwrite(&l,4,1,fil[2]);
				*(short *)&cptr2[24] = 0; //set extra length to 0
				fwrite(&cptr2[0],k+26,1,fil[2]);
				fseek(fil[1],filoffs[1][j],SEEK_SET);
				appendfil(fil[2],fil[1],leng[1]);
				filoffs[2][fcnt[2]] = ftell43;
				filhead[2][fcnt[2]] = filhead[1][j]; fcnt[2]++;
			}
			break;
		}
		if ((boolop != BOOL_AND) && (j >= fcnt[1])) //no match
		{
			score[3]++; if (!quiet) printf("%19d%*c\n",leng[0],19,'-');
			l = LSWAPIB(0x04034b50); fwrite(&l,4,1,fil[2]);
			*(short *)&cptr[24] = 0; //set extra length to 0
			fwrite(&cptr[0],k+26,1,fil[2]);
			fseek(fil[0],filoffs[0][i],SEEK_SET);
			appendfil(fil[2],fil[0],leng[0]);
			filoffs[2][fcnt[2]] = ftell43;
			filhead[2][fcnt[2]] = filhead[0][i]; fcnt[2]++;
		}
		else if (j >= fcnt[1])
		{
			if (boolop == BOOL_AND) { if (!quiet) printf("\t\t\teliminated by /and\n"); }
		}
	}
	if (boolop == BOOL_OR)
	{
		for(j=0;j<fcnt[1];j++)
		{
			cptr2 = &filheadbuf[filhead[1][j]];
			k = SSWAPIB(*(unsigned short *)&cptr2[22]);

			for(i=0;i<fcnt[0];i++)
			{
				cptr = &filheadbuf[filhead[0][i]];

					//compare match
				if (SSWAPIB(*(unsigned short *)&cptr[22]) != k) continue; //filename length
				if (kstrnicmp(&cptr[26],&cptr2[26],k))          continue; //filename
				if (*(long *)&cptr[18] != *(long *)&cptr2[18])  continue; //uncompressed size
				if (*(long *)&cptr[ 6] != *(long *)&cptr2[ 6])  continue; //time/date
				if (*(long *)&cptr[10] != *(long *)&cptr2[10])  continue; //crc32
				break; //match
			}
			if (i < fcnt[0]) continue; //match

			if (!quiet) printf("%-20s",&cptr2[26]);

			ftell43 = ftell(fil[2]);
			leng[1] = LSWAPIB(*(unsigned long *)&cptr2[14]);

			score[3]++; if (!quiet) printf("%*c%20d%c%19d\n",19,'-',leng[1],USEFILECHAR,leng[1],leng[1]);

			l = LSWAPIB(0x04034b50); fwrite(&l,4,1,fil[2]);
			*(short *)&cptr2[24] = 0; //set extra length to 0
			fwrite(&cptr2[0],k+26,1,fil[2]);
			fseek(fil[1],filoffs[1][j],SEEK_SET);
			appendfil(fil[2],fil[1],leng[1]);
			filoffs[2][fcnt[2]] = ftell43;
			filhead[2][fcnt[2]] = filhead[1][j]; fcnt[2]++;
		}
	}

		//Write central file headers
	k = ftell(fil[2]);
	for(i=0;i<fcnt[2];i++)
	{
		cptr = &filheadbuf[filhead[2][i]];

		j = LSWAPIB(0x02014b50); fwrite(&j,4,1,fil[2]);
		s = SSWAPIB(20); fwrite(&s,2,1,fil[2]);
		fwrite(&cptr[0],26,1,fil[2]);
		s = 0; fwrite(&s,2,1,fil[2]);
		s = 0; fwrite(&s,2,1,fil[2]);
		s = 0; fwrite(&s,2,1,fil[2]);
		j = LSWAPIB(0x20); fwrite(&j,4,1,fil[2]); //LSB of Attrib (Don't hardcode!)
		j = LSWAPIB(filoffs[2][i]); fwrite(&j,4,1,fil[2]); //Points to 0x04034b50
		j = SSWAPIB(*(short *)&cptr[22]); //File name length
		fwrite(&cptr[26],j,1,fil[2]);
	}

		//Write end of central dir record
	i = ftell(fil[2]);
	j = LSWAPIB(0x06054b50); fwrite(&j,4,1,fil[2]);
	s = 0; fwrite(&s,2,1,fil[2]);
	s = 0; fwrite(&s,2,1,fil[2]);
	s = SSWAPIB(fcnt[2]); fwrite(&s,2,1,fil[2]);
	s = SSWAPIB(fcnt[2]); fwrite(&s,2,1,fil[2]);
	j = LSWAPIB(i-k); fwrite(&j,4,1,fil[2]);
	j = LSWAPIB(k); fwrite(&j,4,1,fil[2]);
	s = 0; fwrite(&s,2,1,fil[2]);

	fileng[2] = ftell(fil[2]);

	if (!quiet)
	{
		printf("\n\n%d file(s) from %s\n",score[0],filnam[0]);
		printf("%d file(s) from %s\n"    ,score[1],filnam[1]);
		printf("%d file(s) tied\n"       ,score[2]          );
		printf("%d file(s) unmatched\n\n",score[3],filnam[0]);
	}

	if (filespecified == 2)
	{
		if (fil[2]) { fclose(fil[2]); fil[2] = 0; }
		if (fil[0]) { fclose(fil[0]); fil[2] = 0; }
		if ((fileng[2] < fileng[0]) || (boolop != BOOL_NONE))
		{
			remove(filnam[0]);
			rename(filnam[2],filnam[0]);
			if ((boolop == BOOL_NONE) && (!quiet))
			{
				i = ((fileng[0] > fileng[1]) && (!score[3]));
				printf("%s = %s - %d byte(s)\n",filnam[0],filnam[i],fileng[i]-fileng[2]);
			}
		}
		else //modes other than BOOL_NONE require keeping new file no matter what
		{
			remove(filnam[2]);
			if ((boolop == BOOL_NONE) && (!quiet)) printf("no bytes saved :(\n");
		}
	}
	else
	{
		if ((boolop == BOOL_NONE) && (!quiet))
		{
			i = ((fileng[0] > fileng[1]) && (!score[3]));
			printf("%s = %s - %d byte(s)\n",filnam[2],filnam[i],fileng[i]-fileng[2]);
		}
	}
	if (boolop != BOOL_NONE)
	{
		if (!quiet)
		{
			k = 0;
			for(i=filespecified-1;i>=0;i--)
			{
				j = strlen(filnam[i]);
				if (j > k) k = j;
			}

			for(i=0;i<2;i++) printf("%*s%10d bytes input\n",k,filnam[i],fileng[i]);
			if (filespecified == 2) printf("%*s%10d bytes output\n",k,filnam[0],fileng[2]);
									 else printf("%*s%10d bytes output\n",k,filnam[2],fileng[2]);
		}
	}

	quitout(0);
	return(0);
}

#if 0
!endif
#endif
